#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+-------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(C) (32 bytes for SHA256) |
 * +------------+--------------------+-------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */
#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{
    unsigned char* key = malloc(HM_LEN * 2);
        
	if (entropy != NULL)
	{
		//Use hmac with sha512.
    	key = HMAC(EVP_sha512(), KDF_KEY, HM_LEN, (unsigned char*)entropy, entLen, NULL, NULL); 
        memcpy(K->hmacKey, key, HM_LEN);  
        memcpy(K->aesKey, key + HM_LEN, HM_LEN);   
	}
	else
	{
		randBytes(K->hmacKey, HM_LEN);
     	randBytes(K->aesKey, HM_LEN);
	}
	
	/* TODO: write this.  If entropy is given, apply a KDF to it to get
	 * the keys (something like HMAC-SHA512 with KDF_KEY will work).
	 * If entropy is null, just get a random key (you can use the PRF). */
	return 0;
}
size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
}
size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{
	unsigned char* aes_ct = malloc(len);
	unsigned char* hmac;
	size_t ctLen;
	int nWritten = 0;
	
    //Encrypt the plaintext using AES-256 ctr mode encryption.
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	
	if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_ctr(), 0, K->aesKey, IV))
		ERR_print_errors_fp(stderr);
	
	if (1 != EVP_EncryptUpdate(ctx, aes_ct, &nWritten, (unsigned char*)inBuf, len))
		ERR_print_errors_fp(stderr);
		
	EVP_CIPHER_CTX_free(ctx);
	
	//Concatenate the IV with the AES ciphertext.    
	memcpy(outBuf, IV, 16);
	memcpy(outBuf + 16, aes_ct, nWritten);

    //Use hmac with sha256.
	hmac = HMAC(EVP_sha256(), &K->hmacKey, HM_LEN, (unsigned char*)outBuf, (nWritten + 16), NULL, NULL);    
    
    //Concatenate the hmac result with the IV and AES ciphertext.
  	memcpy((outBuf + (16 + nWritten)), hmac, HM_LEN);
	
   //Get the number of bytes written.
    ctLen = nWritten + HM_LEN + 16;
    
    if (ctLen != (ske_getOutputLen(len)))
    {
    	fprintf(stderr, "Invalid number of bytes written.\n");	
    	return 1;
	}
	else
	{
		return ctLen; 
	}
	
	/* TODO: finish writing this.  Look at ctr_example() in aes-example.c
	 * for a hint.  Also, be sure to setup a random IV if none was given.
	 * You can assume outBuf has enough space for the result. */
	/* TODO: should return number of bytes written, which
	             hopefully matches ske_getOutputLen(...). */
}
size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out)
{
    int fd;
    struct stat st;
    unsigned char* pt;
    unsigned char* ctFile;
    
    //Perform mmap file operations for reading plaintext from a file.
    fd = open(fnin, O_RDONLY);
    
    if (fd == -1)
    {
        fprintf(stderr, "Cannot open the encrypt file for reading.\n");	
    	return 1;
    } 
    
    if (fstat(fd, &st) == -1) 
    {
        fprintf(stderr, "fstat Error.\n");	
    	return 1;
    }

    pt = mmap(0, st.st_size, PROT_READ, MAP_SHARED, fd, 0);
    
    if (pt == MAP_FAILED) 
    {
        fprintf(stderr, "mmap Error.\n");	
    	return 1;
    }
     
    //Encrypt the plaintext. 
    size_t ctLen = ske_getOutputLen((size_t)st.st_size);
    unsigned char* ct = malloc(ctLen);
	ske_encrypt(ct,(unsigned char*)pt,(size_t)st.st_size,K,IV);
	
    if (munmap (pt, st.st_size) == -1) 
    {
        fprintf(stderr, "munmap Error.\n");	
    	return 1;
    }
      
    if (close(fd) == -1) 
    {
        fprintf(stderr, "Cannot close the file.\n");	
    	return 1;
    }

    //Perform mmap file operations for writing ciphertext into a file.
    fd = open(fnout, O_RDWR | O_TRUNC);
    
    if (fd == -1)
    {
        fprintf(stderr, "Cannot open the encrypt file for writing.\n");	
    	return 1;
    } 

    if (lseek(fd, ctLen - 1, SEEK_SET) == -1)
    {
       fprintf(stderr, "lseek Error.\n");	
    	return 1;
    }

    if (write(fd, "", 1) == -1)
    {
        fprintf(stderr, "write Error.\n");	
    	return 1;
    } 
    
    ctFile = mmap(0, ctLen, PROT_WRITE, MAP_SHARED, fd, 0);
    
    if (ctFile == MAP_FAILED)
    {
        fprintf(stderr, "mmap Error.\n");	
    	return 1;
    }

    memcpy(ctFile, ct, ctLen);
      
    if (msync(ctFile, ctLen, MS_SYNC) == -1)
    {
        fprintf(stderr, "msync Error.\n");	
    	return 1;             
    }
    
    if (munmap (ctFile, ctLen) == -1) 
    {
        fprintf(stderr, "munmap Error.\n");	
    	return 1;
    }
      
    if (close(fd) == -1) 
    {
        fprintf(stderr, "Cannot close the file.\n");	
    	return 1;
    }
         
	/* TODO: write this.  Hint: mmap. */
	return 0;
}
size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K)
{
	unsigned char* msg1 = malloc(len - HM_LEN);
	unsigned char* msg2 = malloc(HM_LEN);
	unsigned char* iv = malloc(16);
	unsigned char* ct = malloc(len - HM_LEN - 16);
	unsigned char* hmac;
	size_t ctLen = len - HM_LEN - 16;
	int hmacEquals = 0;
	int nWritten = 0;
	
	memcpy(msg1, inBuf, (len - HM_LEN));
	
	hmac = HMAC(EVP_sha256(), &K->hmacKey, HM_LEN, (unsigned char*)msg1, (len - HM_LEN), NULL, NULL);   
	
	memcpy(msg2, &inBuf[len - HM_LEN], HM_LEN);
	
	//Check if the two macs are equal.
	hmacEquals = memcmp(msg2, hmac, HM_LEN);
	
	if (hmacEquals != 0)
    {	
    	return -1;
	}
	
	//Decrypt the ciphertext using AES-256 ctr mode decryption.
	memcpy(iv, inBuf, 16);
	memcpy(ct, &inBuf[16], ctLen);
	
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	
	if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_ctr(), 0, K->aesKey, iv))
		ERR_print_errors_fp(stderr);
		
	if (1 != EVP_DecryptUpdate(ctx, outBuf, &nWritten, ct, ctLen))
		ERR_print_errors_fp(stderr);

	return nWritten;

	/* TODO: write this.  Make sure you check the mac before decypting!
	 * Oh, and also, return -1 if the ciphertext is found invalid.
	 * Otherwise, return the number of bytes written.  See aes-example.c
	 * for how to do basic decryption. */
}
size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in)
{
    int fd;
    struct stat st;
    unsigned char* ct;
    unsigned char* ptFile;
    
    //Perform mmap file operations for reading ciphertext from a file.
    fd = open(fnin, O_RDONLY);
    
    if (fd == -1)
    {
        fprintf(stderr, "Cannot open the decrypt file for reading.\n");	
    	return 1;
    } 
    
    if (fstat(fd, &st) == -1) 
    {
        fprintf(stderr, "fstat Error.\n");	
    	return 1;
    }

    ct = mmap(0, st.st_size, PROT_READ, MAP_SHARED, fd, 0);
    
    if (ct == MAP_FAILED) 
    {
        fprintf(stderr, "mmap Error.\n");	
    	return 1;
     }
     
    //Decrypt the ciphertext.
	size_t ptLen = ((size_t)st.st_size - HM_LEN - 16);
	char* pt = malloc(ptLen);
	ske_decrypt((unsigned char*)pt,ct,((size_t)st.st_size),K);
     
    if (munmap (ct, st.st_size) == -1) 
    {
        fprintf(stderr, "munmap Error.\n");	
    	return 1;
    }
      
    if (close(fd) == -1) 
    {
        fprintf(stderr, "Cannot close the file.\n");	
    	return 1;
    }

    //Perform mmap file operations for writing plaintext into a file.
    fd = open(fnout, O_RDWR | O_TRUNC);
    
    if (fd == -1)
    {
        fprintf(stderr, "Cannot open the decrypt file for writing.\n");	
    	return 1;
    } 
    
    if (lseek(fd, ptLen - 1, SEEK_SET) == -1)
    {
        fprintf(stderr, "lseek Error.\n");	
    	return 1;
    }

    if (write(fd, "", 1) == -1)
    {
        fprintf(stderr, "write Error.\n");	
    	return 1;
    }
    
    ptFile = mmap(0, ptLen, PROT_WRITE, MAP_SHARED, fd, 0);
    
    if (ptFile == MAP_FAILED)
    {
        fprintf(stderr, "mmap Error.\n");	
    	return 1;
    }

    memcpy(ptFile, pt, ptLen);

    if (msync(ptFile, ptLen, MS_SYNC) == -1)
    {
        fprintf(stderr, "msync Error.\n");	
    	return 1;             
    }
    
    if (munmap (ptFile, ptLen) == -1) 
    {
        fprintf(stderr, "munmap Error.\n");	
    	return 1;
    }
      
    if (close(fd) == -1) 
    {
        fprintf(stderr, "Cannot close the file.\n");	
        return 1;
    }
      
	/* TODO: write this. */
	return 0;
}
