/* kem-enc.c
 * simple encryption utility providing CCA2 security.
 * based on the KEM/DEM hybrid model. */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <openssl/sha.h>
#include <sys/stat.h>

#include "ske.h"
#include "rsa.h"
#include "prf.h"

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Encrypt or decrypt data.\n\n"
"   -i,--in     FILE   read input from FILE.\n"
"   -o,--out    FILE   write output to FILE.\n"
"   -k,--key    FILE   the key.\n"
"   -r,--rand   FILE   use FILE to seed RNG (defaults to /dev/urandom).\n"
"   -e,--enc           encrypt (this is the default action).\n"
"   -d,--dec           decrypt.\n"
"   -g,--gen    FILE   generate new key and write to FILE{,.pub}\n"
"   -b,--BITS   NBITS  length of new key (NOTE: this corresponds to the\n"
"                      RSA key; the symmetric key will always be 256 bits).\n"
"                      Defaults to %lu.\n"
"   --help             show this message and exit.\n";

#define FNLEN 255

enum modes {
	ENC,
	DEC,
	GEN
};

/* Let SK denote the symmetric key.  Then to format ciphertext, we
 * simply concatenate:
 * +------------+----------------+
 * | RSA-KEM(X) | SKE ciphertext |
 * +------------+----------------+
 * NOTE: reading such a file is only useful if you have the key,
 * and from the key you can infer the length of the RSA ciphertext.
 * We'll construct our KEM as KEM(X) := RSA(X)|H(X), and define the
 * key to be SK = KDF(X).  Naturally H and KDF need to be "orthogonal",
 * so we will use different hash functions:  H := SHA256, while
 * KDF := HMAC-SHA512, where the key to the hmac is defined in ske.c
 * (see KDF_KEY).
 * */

#define HASHLEN 32 /* for sha256 */

int kem_encrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)
{
	/* TODO: encapsulate random symmetric key (SK) using RSA and SHA256;
	 * encrypt fnIn with SK; concatenate encapsulation and cihpertext;
	 * write to fnOut. */
	 
	 FILE* OutF= fopen(fnOut,"wb");
	 
	 if (!OutF)
	 {
	 	printf("Failed to open the file: %s\n", fnOut);
	 	return -1; 
	 }
	 
	  // Generating  random symmetric key.
	 //size_t rsalen = rsa_numBytesN (K);
	 //unsigned char* x=malloc(rsalen);
	 SKE_KEY skekey;
	// ske_keyGen (&skekey, x, rsalen);
	
	{
        // Test for key generation with entropy.
        unsigned char entropy [512];
        randBytes (entropy, 512);
        ske_keyGen (&skekey, entropy, 512);
    }
	 
	 //allocating buffer in memory for RSA and SHA256 and filling it by zeros
	 size_t rsalen = rsa_numBytesN (K);
	 size_t outBuflen=(rsalen>=HASHLEN)?rsalen:HASHLEN;

	  unsigned char* outBuf= calloc (1,outBuflen);
	 
	 // Encrypting SKE_KEY by RSA and saving to outF.
	 rsa_encrypt (outBuf,(unsigned char*) &skekey, sizeof (skekey),K);
	 
	if (fwrite (outBuf, 1, rsalen, OutF) != rsalen)
    {
        printf ("Failed to write OutF: %s\n", fnOut);
        fclose (OutF);
        free   (outBuf);
        return 0;
    }
	 
	 // Calculating SHA256 for SKE_KEY
	 SHA256((unsigned char*) &skekey, sizeof(skekey),outBuf);
	 char SuccessfulHash=(fwrite (outBuf, 1, HASHLEN, OutF) == HASHLEN);
	 
	 if (!SuccessfulHash)
    {
        printf ("Failed to write outFile: %s\n", fnOut);
        return -1;
    }

	 
	 
	 size_t enc_Result = ske_encrypt_file (fnOut, fnIn, &skekey, NULL, rsalen + HASHLEN);

	 return (enc_Result == -1)? 0: 1;
	 
}



/* NOTE: make sure you check the decapsulation is valid before continuing */
int kem_decrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)
{
	/* TODO: write this. */
	/* step 1: recover the symmetric key */
	/* step 2: check decapsulation */
	/* step 3: derive key from ephemKey and decrypt data. */
	
	FILE* inFile= fopen(fnIn,"rb");
	
	if(!inFile)
	{
		printf("Failed to open inFile:%s\n", fnIn);
		return -1;
	}
	

	size_t rsaLen = rsa_numBytesN (K);
    size_t inBufLen = (rsaLen >= HASHLEN)? rsaLen: HASHLEN;
    
    // Allocating single memory block for data reading
    // and decryption and splitting them into two parts.
    unsigned char* buf   = malloc (inBufLen * 2);
    unsigned char* inBuf = buf, *outBuf = buf + inBufLen;
    
    // Reading SHA256 result form inFile.
    if (fread (inBuf, 1, rsaLen, inFile) != rsaLen)
    {
    	printf("Failed to read inFile:%s\n",fnIn);
    	fclose (inFile);
        free   (buf);
        return 0;
	}
    

    // Decrypting data by RSA to outBuf. 
    // lower part of the outBuf contains SKE_KEY.
    rsa_decrypt (outBuf, inBuf, rsaLen, K);
    
    // Copying SKE_KEY from outBuf.
    SKE_KEY skeKey;
    memcpy (&skeKey, outBuf, sizeof (skeKey));
    
    // Step 2: check decapsulation
    
    // Reading SHA256 result form inFile.
    if (fread (inBuf, 1, HASHLEN, inFile) != HASHLEN)
        printf("Failed to read inFile:%s\n",fnIn);
        
    // Calculating SHA256 for decrypted SKE_KEY
    SHA256 ((unsigned char *) &skeKey, sizeof (skeKey), outBuf);

    // If read and calculated SHA256 don't match, then error.
    if (memcmp (inBuf, outBuf, HASHLEN) != 0)
    {
        printf ("inFile (%s) validation check failed\n", fnIn);
        fclose (inFile);
		free(buf);
    }
    
    fclose (inFile);
	free(buf);
    // Step 3: derive key from ephemKey and decrypt data.
 
    
    size_t dec_Result = ske_decrypt_file (fnOut, fnIn, &skeKey, rsaLen + HASHLEN);
    
    return (dec_Result == -1)? 0: 1;
    


}

/*size_t fsize (FILE *fp) // get the size of  file.
{
    off_t prev = ftello (fp);
    fseeko (fp, 0L, SEEK_END);
    off_t sz = ftello (fp);
    fseeko (fp, prev, SEEK_SET); 
    return (size_t) sz;
}*/
     
int main(int argc, char *argv[]) {
	/* define long options */
	static struct option long_opts[] = {
		{"in",      required_argument, 0, 'i'},
		{"out",     required_argument, 0, 'o'},
		{"key",     required_argument, 0, 'k'},
		{"rand",    required_argument, 0, 'r'},
		{"gen",     required_argument, 0, 'g'},
		{"bits",    required_argument, 0, 'b'},
		{"enc",     no_argument,       0, 'e'},
		{"dec",     no_argument,       0, 'd'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	/* process options: */
	char c;
	int opt_index = 0;
	char fnRnd[FNLEN+1]; // If not set then "/dev/urandom" used by default.
//	fnRnd[FNLEN] = 0;
	char fnIn[FNLEN+1];
	char fnOut[FNLEN+1];
	char fnKey[FNLEN+1];
	
	memset (fnRnd,0,FNLEN+1);
	memset(fnIn,0,FNLEN+1);
	memset(fnOut,0,FNLEN+1);
	memset(fnKey,0,FNLEN+1);
	int mode = ENC;
	#define minNBits 1024
	#define maxNBits 4096
	// size_t nBits = 2048;

	size_t nBits = 1024;
	while ((c = getopt_long(argc, argv, "edhi:o:k:r:g:b:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0],nBits);
				return 0;
			case 'i':
				strncpy(fnIn,optarg,FNLEN);
				break;
			case 'o':
				strncpy(fnOut,optarg,FNLEN);
				break;
			case 'k':
				strncpy(fnKey,optarg,FNLEN);
				break;
			case 'r':
				strncpy(fnRnd,optarg,FNLEN);
				break;
			case 'e':
				mode = ENC;
				break;
			case 'd':
				mode = DEC;
				break;
			case 'g':
				mode = GEN;
				strncpy (fnKey, optarg, FNLEN);
				break;
			case 'b':
				nBits = atol(optarg);
				break;
			case '?':
				printf(usage,argv[0],nBits);
				return 0;
		}
	}

	/* TODO: finish this off.  Be sure to erase sensitive data
	 * like private keys when you're done with them (see the
	 * rsa_shredKey function). */
	 
	 const char *KeyMode;
	 if ((mode==ENC) || (mode==DEC))
	 
	 {
	 	//We have to be sure that all file name are set.
	 	char Error=0;
	 	if(*fnIn==0)
	 	{
	 		printf("Error: input file name is not set.\n");
	 		Error=1;
		 }
		 
		 if(*fnOut==0)
		 {
		 	printf("Error: Output file name is not set.\n");
		 	Error=1;
		 }
		 
		 if(*fnKey==0)
		 {
		 	printf("Error: Key file name is not set.\n");
		 	Error=1;
		 }
		 
		 if(Error) return -1;
		 
		 KeyMode="rb";
	 }
	 else //mode=GEN
	 {
	 	//we have to be sure key file name is set.Also, we have to be sure RSA key length is valid.
	 	char Error=0;
	 	if(*fnKey==0)
		 {
		 	printf("Error: Key file name is not set.\n");
		 	Error=1;
		 }
		 
		 // Rounding key length up to 16 multiples.
		 nBits = (nBits + 15) & (~15);

		 
		 if((nBits < minNBits)|| (nBits> maxNBits))
		 {
		 	printf("Error: RSA Key length :%d out of range [%d, %d].\n",
			  nBits, minNBits, maxNBits);
		 	Error=1;
		 }
		 
		 if ( Error) return -1;
		 
		 KeyMode="wb";
	 	
	 }
	 if (*fnRnd!=0)
	 {
	 	// If random generator seed file set, then
        //  we use it for random generator initialization.
	 	FILE *entropyf= fopen(fnRnd,"rb");
	 	
	 	if (!entropyf)
	 	{
	 		printf("Error: Random generator seed file open error.\n");
	 		return 1;
		 }
		 
		size_t entropylen= fsize(entropyf);
		 unsigned char *entropy= malloc( entropylen);
		 size_t readlen=fread(entropy,1,entropylen,entropyf);
		 fclose(entropyf);
		 
		 if (readlen!=entropylen)
		 {
		 	printf("Error: reading entropy file:%s\n.",fnRnd);
		 	free(entropy);
		 	return 1;
		 	
		 }
		 
		 setSeed(entropy,entropylen);
		 free(entropy);
	}
	else //if fnRnd==0
	{
		// Seed file name is not set, initializing
        // random generator from "/dev/urandom".
		setSeed(NULL,0);

	}
	
	RSA_KEY K;
	int result=0;
	FILE* rsakfile= fopen(fnKey, KeyMode);
	
	if (!rsakfile)
	{
		printf ("Error opening RSA key file:%s\n", fnKey);
		return 1;
	}
	
	switch (mode) {
		case ENC:
			rsa_readPublic(rsakfile,&K);
			result= kem_encrypt (fnOut, fnIn, &K);
			break;
		case DEC:
			rsa_readPrivate (rsakfile, &K);
			result = kem_decrypt (fnOut, fnIn, &K);
			break;
		case GEN:
			rsa_keyGen (nBits, &K);
            rsa_writePrivate (rsakfile, &K);
            result = (ferror (rsakfile) == 0)? 1: 0;
            
             if (result)
            {
                // Writing public key, appending ".pub" to file name
                char pubKeyFile [1024];
                strcpy (pubKeyFile, fnKey);
                strcat (pubKeyFile, ".pub");

                FILE* rsaKeyFile2 = fopen (pubKeyFile, KeyMode);

                if (!rsaKeyFile2)
                {
                    printf ("Error opening public Rsa Key File: %s\n", pubKeyFile);
                    return 1;
                }

                rsa_writePublic (rsaKeyFile2, &K);
                result = (ferror (rsaKeyFile2) == 0)? 1: 0;
                fclose (rsaKeyFile2);
            }

            break;
            
	}
	// Destroying RSA key in memory.

	rsa_shredKey (&K);
	fclose (rsakfile);
	
	if (mode==GEN)
	{
		if(!result)
		{
			printf ("Error: failed to write key files: %s, %s.pub.\n", fnKey, fnKey);
			return 1;
		}
		
		printf ("Key files successfully generated: %s, %s.pub.\n", fnKey, fnKey);
        return 0;
	}
	else
	{
		if(!result)
		{
			printf("Failed to %s file.\n", (mode==ENC)?"encrypted":"decrypted");
			return 1;
		}
		
		printf("File %s successfully %s to %s.\n",fnIn, ((mode==ENC)?"encrypted":"decrypted"),fnOut);
			return 0;
	}
	
}
